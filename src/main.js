import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 导入element ui 组件
// 导入时间线样式
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 导入全局样式表
import './assets/global.css'
// 导入axios
import axios from 'axios'
// 导入副文本编辑器
import VueQuillEditor from 'vue-quill-editor'

import TreeTable from 'vue-table-with-tree-grid'

// 导入NProgress 进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 导副文本编辑器对应的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
Vue.prototype.$http = axios

// 为axios设置拦截器,设置权限
axios.interceptors.request.use(config => {
  NProgress.start()
  // 为请求头header设置Authorization
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 最后必须return
  return config
})

axios.interceptors.response.use(config => {
  NProgress.done()
  return config
})
// Vue.use(TreeTable)
Vue.component('tree-table', TreeTable)

Vue.filter('dateFormat', function (originVal) {
  const dt = new Date(originVal)

  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '0')

  const hh = (dt.getHours() + '').padStart(2, '0')
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  const ss = (dt.getSeconds() + '').padStart(2, '0')

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

Vue.use(ElementUI)

Vue.use(VueQuillEditor)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
